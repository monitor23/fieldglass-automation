*** Settings ***
Documentation   This Keywords Page is for the Login Functionality for the Login Page.
Library         SeleniumLibrary
Resource        ./Login_PO.robot
*** Keywords ***
Open Fieldglass Portal Login Page
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}    open browser    ${URL}   ${BROWSER}
    Maximize Browser Window
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}    title should be   SAP Fieldglass Login
    sleep  2s

Validate Login Page Loaded SuccessFully
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   SAP Fieldglass provides the industry's leading cloud technology for services procurement and external workforce management.
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Welcome to
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   SAP Fieldglass
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Username
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Password

Login To the FieldGlass Portal With User Name and Password
    [Documentation]  This keyword is for Login
    [Arguments]  ${USERNAME}=${USERNAME}   ${PASSWORD}=${PASSWORD}
    Enter User Name     ${USERNAME}
    Enter Password      ${PASSWORD}
    Click to Signin
    Validate Dashboard Loaded With Success

Validate Dashboard Loaded With Success
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   My Work Items
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Home
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Welcome
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Job Postings with Recent Activity
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Statements of Work

Logout From FieldGlass
    Click to logout button
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   Thank you for using SAP Fieldglass.
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  page should contain   You are now signed out of your session.
