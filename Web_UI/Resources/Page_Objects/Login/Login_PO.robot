*** Settings ***
Documentation   This Keywords Page is for the Login Functionality for the Login Page Objects.
Library         SeleniumLibrary
*** Variables ***
${USERNAME_BOX}     //*[@id="usernameId_new"]
${PASS_BOX}         //*[@id="passwordId_new"]
${SIGN_IN_BUTTON}   //*[@id="primary_content"]/div[4]/button
*** Keywords ***
Enter User Name
    [Arguments]     ${USERNAME}
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  input text    ${USERNAME_BOX}    ${USERNAME}

Enter Password
    [Arguments]     ${PASSWORD}
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  input text    ${PASS_BOX}    ${PASSWORD}

Click to Signin
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  click element    ${SIGN_IN_BUTTON}

Click to logout button
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  click element    //*[@id="accountOpener"]/div[1]/div/div/span
    wait until keyword succeeds  ${TIMEOUT}   ${RETRY_TIME}  click element    //*[@id="signOut"]/div