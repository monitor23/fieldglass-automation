*** Settings ***
Documentation     A test suite with a Fieldglass Login Template Scenario
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource            ../Resources/Page_Objects/Login/Login.robot

*** Test Cases ***
FieldGlass Portal Login And Logout With Right Credentials
    [Documentation]
    [Tags]    Fieldglass    SMOKE
    [Setup]   Log  Starting Testcase ${TEST NAME}
    Open Fieldglass Portal Login Page
    Validate Login Page Loaded SuccessFully
    Login To the FieldGlass Portal With User Name and Password
    Logout From FieldGlass
	log   Finished Test Steps
    [Teardown]    run keywords  log   Closing Test Case ${TEST NAME}   AND   Close Browser